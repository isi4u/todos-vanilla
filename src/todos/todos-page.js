import './todo-list.js'

class TodosPage extends HTMLElement {
    constructor() {
      super();
    }

    handleTogglePage(event) {
        if (event.detail.show) {
            this.userName.innerHTML = event.detail.data.userName;
            this.todoList.dispatchEvent(new CustomEvent('changeUser', {
                detail: {
                    userId: event.detail.data.userId,
                    userName: event.detail.data.userName
                }
            }))
            this.style.display = 'block';
        } else {
            this.style.display = 'none';
        }
    }

    handleBackBtn(event) {
        document.dispatchEvent(new CustomEvent('showPage', {
            detail: {
                page: 'users'
            }
        }))
    }
  
    connectedCallback() {
        this.style.display = 'none';
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
            :host {
                margin: 30px auto;
                display: block;
                width: 80%;
            }
        </style>
        `;
        this.backBtn = document.createElement('button')
        this.backBtn.style.cursor = 'pointer'
        this.backBtn.innerHTML = 'Назад'
        this.shadowRoot.append(this.backBtn)

        this.userName = document.createElement('h2')
        this.shadowRoot.append(this.userName)

        this.todoList = document.createElement('todo-list')
        this.shadowRoot.append(this.todoList)

        this.addEventListener('togglePage', this.handleTogglePage)
        this.backBtn.addEventListener('click', this.handleBackBtn)
    }
  
    disconnectedCallback() {
        this.removeEventListener('togglePage', this.handleTogglePage)
        this.backBtn.removeEventListener('click', this.handleBackBtn)
    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('todos-page', TodosPage);