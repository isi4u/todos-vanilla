import './todo-item.js'

class TodoList extends HTMLElement {
    constructor() {
      super();
     
    }

    fetchData(userId) {
      const config = {
          headers: {
              'Accept': 'application/json'
          }
      }
      fetch(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`, config)
      .then((res) => res.json())
      .then((data) => {
          let itemDelay = 0;
          let itemTransformX = 70;
          data.forEach(item => {
              this.todosContainer.insertAdjacentHTML('beforeEnd', `
              <todo-item
                item-transform-x="${itemTransformX}"
                item-delay="${itemDelay}"
                todo-title="${item.title}"
                todo-completed="${item.completed}"
                >
              </todo-item>
              `);
              itemDelay += 0.1;
              itemTransformX = itemTransformX * -1;
          });
          
      })
  }

  handleChangeUser(event) {
    this.todosContainer.innerHTML = '';
    this.fetchData(event.detail.userId)
  }
  
    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
          todo-item:nth-child(odd) {
            background-color: #f9f9f9;
          }
        </style>
        `;
        this.todosContainer = document.createElement('div')
        this.shadowRoot.append(this.todosContainer)
        this.addEventListener('changeUser', this.handleChangeUser)
        
    }
  
    disconnectedCallback() {

    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('todo-list', TodoList);
