import "./../UI/checkbox-svg.js"

class TodoItem extends HTMLElement {
    constructor() {
      super();

    }

 
    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
            :host {
                display: grid;
                grid-template-columns: 1fr 20px;
                margin: 5px 0;
                padding: 5px 10px 2px;
            }
            .todo-title {
                padding-right: 30px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .todo-completed {
                padding-left: 10px;
                text-align: right;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            :host {
                transform: translate(${this.getAttribute('item-transform-x')}px, 10px);
                opacity: 0;
                transition: all 0.3s ease ${this.getAttribute('item-delay')}s;
            }
        </style>
        <span class="todo-title">${this.getAttribute('todo-title') || 'Без названия'}</span><checkbox-svg completed="${this.getAttribute('todo-completed')}"></checkbox-svg>
        `;
        setTimeout(() => {
            this.style.transform = 'translate(0px, 0px)';
            this.style.opacity = 1;
        }, 100)
        

    }
  
    disconnectedCallback() {

    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('todo-item', TodoItem);