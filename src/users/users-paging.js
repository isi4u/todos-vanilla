class UsersPaging extends HTMLElement {
    constructor() {
        super();
        this.handleClick = (event) => {
        document.dispatchEvent(new CustomEvent('changeUsersPage', {
                detail: {
                    pageId: event.currentTarget.getAttribute('page-id')
                }
            }))
            this.changeCurrPageId(parseInt(event.currentTarget.getAttribute('page-id')))
        }
    }

    

    changeCurrPageId(id) {
        this.currId = parseInt(id)
        this.currBtn.innerHTML = this.currId
        this.beginBtn.setAttribute('page-id', (this.currId == 1 ? 1 : this.currId - 1))
        this.currBtn.setAttribute('page-id', this.currId)
    }
  
    connectedCallback() {
        this.currId = parseInt(this.getAttribute('page-id'));
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
            span {
                padding: 5px 10px;
                cursor: pointer;
                background-color: #ccc;
                margin: 5px 1px;
                width: 20px;
                display: inline-block;
                text-align: center;
            }
        </style>
        
        `;
        this.beginBtn = document.createElement('span')
        this.beginBtn.setAttribute('page-id', 1)
        this.beginBtn.innerHTML = '<<'
        this.beginBtn.addEventListener('click', this.handleClick)
        this.shadowRoot.append(this.beginBtn)

        this.prevBtn = document.createElement('span')
        this.prevBtn.setAttribute('page-id', (this.currId == 1 ? 1 : this.currId - 1))
        this.prevBtn.innerHTML = '<'
        this.prevBtn.addEventListener('click', this.handleClick)
        this.shadowRoot.append(this.prevBtn)

        this.currBtn = document.createElement('span')
        this.currBtn.setAttribute('page-id', this.currId)
        this.currBtn.innerHTML = this.currId
        this.currBtn.addEventListener('click', this.handleClick)
        this.shadowRoot.append(this.currBtn)

        this.nextBtn = document.createElement('span')
        this.nextBtn.setAttribute('page-id', (this.currId == this.lastId ? this.lastId : this.currId + 1))
        this.nextBtn.innerHTML = '>'
        this.nextBtn.addEventListener('click', this.handleClick)
        this.shadowRoot.append(this.nextBtn)

        this.endBtn = document.createElement('span')
        this.endBtn.setAttribute('page-id', this.lastId)
        this.endBtn.innerHTML = '>>'
        this.endBtn.addEventListener('click', this.handleClick)
        this.shadowRoot.append(this.endBtn)


    }
  
    disconnectedCallback() {
        this.beginBtn.removeEventListener('click', this.handleClick)
        this.prevBtn.removeEventListener('click', this.handleClick)
        this.currBtn.removeEventListener('click', this.handleClick)
        this.nextBtn.removeEventListener('click', this.handleClick)
        this.endBtn.removeEventListener('click', this.handleClick)
    }
  
    static get observedAttributes() {
      return ['page-id', 'last-page-id'];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {
      if(name == 'page-id') {
          if (oldValue) {
            this.changeCurrPageId(newValue)
          }
      } else if(name == 'last-page-id'){
        this.lastId = parseInt(newValue)
        this.nextBtn.setAttribute('page-id', (this.currId == this.lastId ? this.lastId : this.currId + 1))
        this.endBtn.setAttribute('page-id', this.lastId)
      }
    }

}

customElements.define('users-paging', UsersPaging);