import './user-item.js'
import './users-paging.js'


class UserList extends HTMLElement {
    constructor() {
      super();
      this.pageId = 1;
      this.handleChangeUsersPage = (event) => {
        this.fetchData(event.detail.pageId)
      }
    }

    fetchData(pageId) {
        const config = {
            headers: {
                'Accept': 'application/json'
            }
        }
        fetch(`https://jsonplaceholder.typicode.com/users?_page=${pageId}&_limit=5`, config)
        .then((res) => {
            if (!this.paging) {
              this.paging = document.createElement('users-paging')
              this.paging.setAttribute('page-id', 1)
              this.shadowRoot.append(this.paging)
            }
            this.paging.setAttribute('last-page-id', Math.ceil(res.headers.get('x-total-count') / 5))
            return res.json()
        })
        .then((data) => {
            this.usersContainer.innerHTML = '';
            let itemDelay = 0;
            let itemTransformX = 70;
            data.forEach(item => {
                this.usersContainer.insertAdjacentHTML('beforeEnd', `
                <user-item
                item-transform-x="${itemTransformX}"
                item-delay="${itemDelay}"
                user-id="${item.id}"
                user-name="${item.name}"
                user-email="${item.email}"
                user-phone="${item.phone}"
                ></user-item>
                `);
                itemDelay += 0.1;
                itemTransformX = itemTransformX * -1;
            });
            
        })
    }

  
    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
          :host {

          }
          user-item:nth-child(odd) {
              background-color: #f9f9f9;
          }
        </style>
        `;
        this.usersContainer = document.createElement('div')
        this.shadowRoot.append(this.usersContainer)
        this.fetchData(this.pageId)
        document.addEventListener('changeUsersPage', this.handleChangeUsersPage)
    }
  
    disconnectedCallback() {
      document.removeEventListener('changeUsersPage', this.handleChangeUsersPage)
    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('user-list', UserList);