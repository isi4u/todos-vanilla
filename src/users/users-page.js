import './user-list.js'
class UsersPage extends HTMLElement {
    constructor() {
      super();

    }

    handleTogglePage(event) {
        if (event.detail.show) {
            this.style.display = 'block';
        } else {
            this.style.display = 'none';
        }
    }
  
    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
          :host {
            margin: 30px auto;
            display: block;
            width: 80%;
          }
        </style>
        <h2>Список пользователей</h2>
        <user-list></user-list>
        `;
        this.addEventListener('togglePage', this.handleTogglePage)
    }
  
    disconnectedCallback() {
        this.removeEventListener('togglePage', this.handleTogglePage)
    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('users-page', UsersPage);