class UserItem extends HTMLElement {
    constructor() {
      super();

    }

    handleClick(event) {
        document.dispatchEvent(new CustomEvent('showPage', {
            detail: {
                page: 'todos',
                userName: event.currentTarget.getAttribute('user-name') || '',
                userId: event.currentTarget.getAttribute('user-id')
            }
        }))
    }
  
    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
        <style>
            :host {
                display: grid;
                grid-template-columns: 1fr 180px 180px;
                margin: 5px 0;
                cursor: pointer;
                padding: 5px 10px;
                transform: translate(${this.getAttribute('item-transform-x')}px, 10px);
                opacity: 0;
                transition: all 0.3s ease ${this.getAttribute('item-delay')}s;
            }
            
            .user-name {
                padding-right: 30px;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .user-email {
                padding: 0px 10px;

                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
            .user-phone {
                padding-left: 10px;
                text-align: right;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
            }
        </style>
        <span class="user-name">${this.getAttribute('user-name') || 'Без имени'}</span><span title="${this.getAttribute('user-email')}" class="user-email">${this.getAttribute('user-email') || 'Эл.почта не задана'}</span><span title="${this.getAttribute('user-phone')}" class="user-phone">${this.getAttribute('user-phone') || 'Телефон не задан'}</span>
        `;

        setTimeout(() => {
            this.style.transform = 'translate(0px, 0px)';
            this.style.opacity = 1;
        }, 100)

        this.addEventListener('click', this.handleClick)
    }
  
    disconnectedCallback() {
        this.removeEventListener('click', this.handleClick)
    }
  
    static get observedAttributes() {
      return [];
    }
  
    attributeChangedCallback(name, oldValue, newValue) {

    }

}

customElements.define('user-item', UserItem);