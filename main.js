import './style.css'

// Math.__proto__.add2 = value => value + 2
// Math.add3 = value => value + 3

import './src/users/users-page.js'
import './src/todos/todos-page.js'

const pages = {};
pages['users'] = document.createElement('users-page');
document.body.append(pages['users'])

const options = {
  page: 'users'
}

document.addEventListener('showPage', event => {
  pages[options.page].dispatchEvent(new CustomEvent('togglePage', {
    detail: {
      show: false,
    }
  }))
  options.page = event.detail.page;
  if (!pages.hasOwnProperty(options.page)) {
    pages[options.page] = document.createElement(`${options.page}-page`);
    document.body.append(pages[options.page])
  }
  pages[options.page].dispatchEvent(new CustomEvent('togglePage', {
    detail: {
      show: true,
      data: event.detail
    }
  }))
})



